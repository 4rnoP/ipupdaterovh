## ipUpdaterOVH
This small script is used to update the target IP of my domain rented at ovh.com.
Theorically, DynDns should do the job, but it's buggy as hell for me (I tried ddclient, curl command, ...).

So, instead, I use OVH API to get what I want.

### Pre-required
You need to install ovh, requests and PyYaml packages in order to execute the script.

For example, with `pip`
```shell script
pip install ovh
pip install requests
pip install PyYaml
```

### Usage
```shell script
python ./ovh-dns-update.py <config file>
```
Without specify the config file path, it set it at `~/.config/ovh-dns-update.yml`

### Configuration
This is an example configuration, you can add as many domains and subdomains you want.

To redirect domain itself, add an sub-domain with a empty string.

To create the authentification keys, go [here](https://api.ovh.com/createToken/index.cgi?GET=/*&PUT=/*&POST=/*&DELETE=/*).

You can change the web service who is retriving your public ip address, the only contraint is that it need to only respond the ip, without any text around.
```yaml
auth:
  endpoint: 'ovh-eu'
  application_key: 'my app key'
  application_secret: 'my_app_secret'
  consumer_key: 'my_consumer_key'

ip_retrieve: 'https://api.ipify.org/'

domains:
  "mydomain.com":
    - ""
    - "nextcloud"
    - "wordpress"
    - "grav"
```