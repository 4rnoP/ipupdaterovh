# -*- encoding: utf-8 -*-

import ovh
import requests
import yaml
import sys

config_path = sys.argv[1] if len(sys.argv) > 1 else "~/.config/ovh-dns-update.yml"
print(config_path)
with open(config_path, "r") as file:
    data = file.read()
    config = yaml.safe_load(data)

client = ovh.Client(
    endpoint=config["auth"]["endpoint"],
    application_key=config["auth"]["application_key"],
    application_secret=config["auth"]["application_secret"],
    consumer_key=config["auth"]["consumer_key"]
)

my_ip = requests.get(config["ip_retrieve"]).text
for domain in config["domains"]:
    subdomains = config["domains"][domain]
    for subdomain in subdomains:
        id = client.get('/domain/zone/{domain}/record'.format(domain=domain), fieldType='A', subDomain=subdomain)
        if id:
            result = client.put('/domain/zone/{domain}/record/{id}'.format(domain=domain, id=id[0]), target=my_ip)
            print("{subdomain}.{domain} updated to {my_ip}".format(domain=domain, subdomain=subdomain, my_ip=my_ip))
        else:
            no_subdomain = ValueError(subdomain + " is not registered on the OVH DNS")
            raise no_subdomain
    client.post('/domain/zone/{domain}/refresh'.format(domain=domain))
    print("{domain} refreshed !".format(domain=domain))